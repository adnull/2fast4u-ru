import time
import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder

from . import utilities

class User:
	def __init__(self, cookies):
		self.cookies = cookies
		self.steamid_64 = self.cookies["steamLoginSecure"].split("%")[0]
		self.username = utilities.random_string(10)

	def claim(self, target_id):
		mp_encoder = MultipartEncoder(fields={
			"sessionID": self.session_id,
			"type": "profileSave",
			"personaName": "2fast4u",
			"real_name": "",
			"customURL": target_id,
			"country": "",
			"state": "",
			"city": "",
			"summary": "No information given.",
			"json": "1"
		}, encoding="utf-8")

		headers = {
			"Content-Type": mp_encoder.content_type
		}

		resp = requests.post(
			f"https://steamcommunity.com/profiles/{self.steamid_64}/edit",
			allow_redirects=False,
			data=mp_encoder,
			headers=headers,
			cookies=self.cookies
		)

		return resp.json().get("success", 0) == 1

	def loop_target(self, target_id, delay=0, max_attempts=5):
		for _ in range(max_attempts):
			if self.claim(target_id):
				return True
			time.sleep(delay)
		return False
