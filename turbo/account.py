class Account:
	def __init__(self, username, password):
		self.username = username
		self.password = password

	@classmethod
	def from_combo(cls, combo):
		data = combo.split(":", 1)
		return cls(data[0], data[1])
