import time
import base64
import requests
from Cryptodome.PublicKey import RSA
from Cryptodome.Cipher import PKCS1_v1_5

class Steam:
	def __init__(self, accounts, proxies, data):
		self.accounts = accounts
		self.proxies = proxies
		self.data = data

	def login(self,
		username,
		password,
		emailauth=None,
		twofactorcode=None,
		emailsteamid=None,
		captcha_gid=None,
		captcha_text=None
	):
		session = requests.Session()
		session.proxies = self.proxies.new()

		getrsa_payload = {
			"donotcache": round(time.time() * 1000),
			"username": username
		}

		resp = session.post("https://steamcommunity.com/login/getrsakey/", data=getrsa_payload)

		mod = int(resp.json()["publickey_mod"], 16)
		exp = int(resp.json()["publickey_exp"], 16)
		rsa = RSA.construct((mod, exp))
		cipher = PKCS1_v1_5.new(rsa)

		encrypted_password = base64.b64encode(cipher.encrypt(password.encode("utf-8")))
		rsatimestamp = resp.json()["timestamp"]

		resp = session.post("https://steamcommunity.com/login/dologin/", data={
			"username": username,
			"password": encrypted_password,
			"rsatimestamp": rsatimestamp,
			"remember_login": True,
			"emailauth": emailauth,
			"captchagid": captcha_gid,
			"captcha_text": captcha_text,
			"emailsteamid": emailsteamid,
			"twofactorcode": twofactorcode,
			"donotcache": time.time() * 1000
		})

		data = resp.json()

		if not data:
			return None, None

		# `welcomed=1` to get past the "welcome to community" page
		session.get("https://steamcommunity.com/my/edit?welcomed=1")

		return data.get("success", False), session.cookies.get_dict()

	def thread(self):
		while len(self.accounts) > 0:
			account = self.accounts.pop()
			success, cookies = self.login(account.username, account.password)

			if success is None:
				print(f"Error occured logging in to '{account.username}'")
				self.accounts.append(account)
				continue

			if not success:
				print(f"Failed to log in to '{account.username}'")
				continue

			print(f"Logged into '{account.username}' successfully")

			self.data.append({
				"username": account.username,
				"cookies": cookies
			})

		return
