import json
import threading
import collections

from .account import Account
from .proxy import Proxies
from .steam import Steam
from .user import User

class Turbo:
	def __init__(self,
		accounts="accounts.txt",
		proxies="proxies.txt",
		cookies="cookies.json",
		threads=5
	):
		self.accounts = accounts
		self.cookies = cookies
		
		with open(proxies, "r", encoding="utf-8") as file:
			self.proxies = Proxies(collections.deque(file.read().split("\n")))

		self.threads = threads
		self.data = collections.deque([])
		self.claimed = False

	def unchecked(self):
		with open(self.accounts, "r", encoding="utf-8") as file:
			usernames = file.read().split("\n")

		with open(self.cookies, "r", encoding="utf-8") as file:
			cookies = json.loads(file.read())

		validated = []

		for combo in usernames:
			exists = False
			username = combo.split(":")[0]

			for cookie in cookies:
				if username == cookie["username"]:
					exists = True
					break

			if not exists:
				validated.append(combo)

		return validated

	def login(self, out):
		accounts = collections.deque([Account.from_combo(x) for x in self.unchecked()])

		print(f"Logging in to {str(len(accounts))} accounts...")

		client = Steam(accounts, self.proxies, self.data)
		threads = []

		for _ in range(self.threads):
			thread = threading.Thread(target=client.thread)
			thread.start()
			threads.append(thread)

		for thread in threads:
			thread.join()

		with open(out, "r", encoding="utf-8") as file:
			items = json.loads(file.read())

		for item in self.data:
			items.append(item)

		with open(out, "w+", encoding="utf-8") as file:
			file.write(json.dumps(items))

	def target_thread(self, users, url):
		while not self.claimed:
			users.rotate(1)
			if users[0].loop_target(url):
				print(f"Successfully claimed the ID on `{users[0].steamid_64}`")
				self.claimed = True

		return

	def target(self, url):
		with open(self.cookies, "r", encoding="utf-8") as file:
			users = collections.deque([User(x["cookies"]) for x in json.loads(file.read())])

		threads = []

		for _ in range(self.threads):
			thread = threading.Thread(target=self.target_thread, args=(users, url,))
			thread.start()
			threads.append(thread)

		for thread in threads:
			thread.join()
