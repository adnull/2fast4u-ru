import argparse

from .turbo import Turbo

def main():
	parser = argparse.ArgumentParser(description="Multi-threaded Steam community URL turbo with proxy support.")

	parser.add_argument("mode", type=str, help="`login` or `target`, whether to login to a list of accounts or target a community url")

	parser.add_argument("-accounts", type=str, default="accounts.txt", help="file to load accounts from")
	parser.add_argument("-proxies", type=str, default="proxies.txt", help="file to load proxies from, in `user:pass@ip:port` format")
	parser.add_argument("-cookies", type=str, default="cookies.json", help="where to save cookies to")
	parser.add_argument("-target", type=str, default=None, help="steam community url to target")
	parser.add_argument("-threads", type=int, default=5, help="amount of threads to use")

	args = parser.parse_args()

	turbo = Turbo(args.accounts, args.proxies, args.cookies, args.threads)

	if args.mode == "login":
		turbo.login(args.cookies)
	elif args.mode == "target":
		turbo.target(args.target)
	else:
		print("Invalid mode was given (login, target)")

if __name__ == "__main__":
	main()
