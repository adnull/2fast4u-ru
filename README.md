# 2fast4u.ru
Steam turbo that works best threaded with many accounts and proxies. *This project is a WIP and has yet to be tested completely.*
## Arguments
```
C:\2fast4u>py -m turbo --help
usage: __main__.py [-h] [-accounts ACCOUNTS] [-proxies PROXIES] [-cookies COOKIES] [-target TARGET] [-threads THREADS]
                   mode

Multi-threaded Steam community URL turbo with proxy support.

positional arguments:
  mode                `login` or `target`, whether to login to a list of accounts or target a community url

optional arguments:
  -h, --help          show this help message and exit
  -accounts ACCOUNTS  file to load accounts from
  -proxies PROXIES    file to load proxies from, in `user:pass@ip:port` format
  -cookies COOKIES    where to save cookies to
  -target TARGET      steam community url to target
  -threads THREADS    amount of threads to use
```
## Process
The `login` mode will iterate over all Steam accounts in a file, logging in to each of them and saving their cookies to a new file in the following format.
```
[
	{
		"username": "account1234",
		"cookies": {
			"sessionid": "1234",
			"steamLoginSecure": "steamid%1234",
			... more cookies ...
		}
	}
]
```
By switching the mode to `target`, the program will then start several threads. These threads will rotate over all given accounts indefinitely, attempting to claim the desired URL 5 times in a row before moving accounts. Once the URL is claimed, the program will shut down, printing out the username of the account that claimed the URL.
## Notes
### Login validation
Adding more login validation (CAPTCHA, 2FA) across different threads will most likely be pretty difficult and unnecessary. Just use fresh accounts that can login from anywhere and you'll be fine.

In the future I could see about adding this possibly in a web panel.
### Necessary cookies
Only the `sessionid` and `steamLoginSecure` cookies are necessary to change profile data, although we save all just in case we may need them in the future.