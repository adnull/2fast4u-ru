import requests

# Todo: class Proxy() that indicates whether a proxy has been validated previously.

class Proxies:
	def __init__(self, list):
		self.proxies = list
		self.original_ip = requests.get("https://httpbin.org/get").json()["origin"]

	def validate(self, proxy):
		try:
			resp = requests.get("https://httpbin.org/get", proxies={
				"http": f"http://{proxy}",
				"https": f"https://{proxy}"
			})

			return resp.json()["origin"] != self.original_ip
		except Exception:
			return False

	def new(self):
		found = False

		if len(self.proxies) < 1:
			raise Exception("No usable proxies could be found")

		while not found:
			self.proxies.rotate(1)
			proxy = self.proxies.pop() # Thread safety

			if self.validate(proxy):
				self.proxies.append(proxy)
				found = proxy

		print("Found a working proxy: " + found)
		return found
